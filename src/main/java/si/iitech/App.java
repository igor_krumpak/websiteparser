package si.iitech;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import si.iitech.parser.ArticleParserService;

import java.util.Timer;


/**
 * Hello world!
 */
public class App {

    private static Logger logger = Logger.getLogger(App.class);

    public static void main(String[] args) {
        setupArticleParser();
    }

    private static void setupArticleParser() {
        logger.log(org.apache.log4j.Level.INFO, "setupArticleParser");
        ArticleParserService parserService = BeanContext.INSTANCE.getBean(ArticleParserService.class);
        Timer timer = new Timer(false);
        timer.scheduleAtFixedRate(parserService, 0, 60 * 1000);
    }




}
