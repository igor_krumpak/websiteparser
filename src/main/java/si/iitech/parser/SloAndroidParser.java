/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package si.iitech.parser;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import si.iitech.model.Article;
import si.iitech.model.ArticleMedia;

import javax.inject.Singleton;
import java.util.*;

/**
 * @author Igor
 */
@Singleton
public class SloAndroidParser extends WebSiteParser {

    private static final Logger logger = Logger.getLogger(SloAndroidParser.class);

    @Override
    protected String webSiteURL() {
        return "http://slo-android.si";
    }

    public List<Article> parseAllArticles() {
        List<Article> articleList = new ArrayList<>();
        try {
            Document document = super.readWebSite(webSiteURL() + "/prispevki/novice.html", false);
            Elements titleElements = document.select(".catItemTitle");
            Elements hrefElemenets = titleElements.select("a");
            for (Element e : hrefElemenets) {
                Article article = returnArticle(webSiteURL() + e.attr("href"));
                if (article != null) {
                    logger.log(Level.INFO, article.toJsonString());
                    articleList.add(article);
                }
            }
            return articleList;
        } catch (Exception e) {
            logger.log(Level.ERROR, "SloAndroid parseAllArticles" + e.toString());
            return articleList;
        }
    }

    public Article returnArticle(String href) {
        try {
            Document document = super.readWebSite(href, false);
            Article article = new Article();

            //HREF
            article.setHref(href);

            //OVERLAY
            Elements overlayElements = document.select(".itemBody");

            //TITLE
            Elements titleElement = document.select(".itemTitle");
            String title = titleElement.text();
            article.setTitle(title);

            //AUTHOR AND DATE
            Elements authorAndDateElement = document.select(".itemAuthor");
            String authorAndDate = authorAndDateElement.text();
            String[] authorAndDateTexts = authorAndDate.split("\\s+");

            //AUTHOR
            List<String> authors = new ArrayList<String>();
            authors.add(authorAndDateTexts[1]);
            article.setAuthors(authors);
            article.setAuthors(authors);

            //DATE
            article.setArticleDate(formatSloAndroidDate(authorAndDateTexts[3], authorAndDateTexts[4], authorAndDateTexts[5], authorAndDateTexts[7]));


            //SOURCE
            article.setSource(Article.Source.SLO_ANDROID);

            //VIDEOS
            Elements videoElements = overlayElements.select(".itemVideoEmbedded");
            ArrayList<ArticleMedia> videoList = new ArrayList<>();
            if (!videoElements.isEmpty()) {

                for (Element videoElement : videoElements) {
                    ArticleMedia video = new ArticleMedia();
                    String videoHref = videoElement.select("iframe").attr("src");
                    video.setHref(videoHref.substring(2));
                    videoList.add(video);
                }
            }
            article.setVideos(videoList);

            //IMAGES
            Elements imageElements = overlayElements.select("img");
            ArrayList<ArticleMedia> imageList = new ArrayList<>();
            if (!imageElements.isEmpty()) {
                for (Element imageElement : imageElements) {
                    ArticleMedia image = new ArticleMedia();
                    String imageHref = imageElement.attr("src");
                    String imageTitle = imageElement.attr("alt");
                    if (imageHref.startsWith("http:") == true) {
                        image.setHref(imageHref);
                    } else {
                        image.setHref("http://slo-android.si" + imageHref);
                    }

                    image.setTitle(imageTitle);
                    imageList.add(image);
                }
            }
            article.setImages(imageList);

            //SOURCES
            List<ArticleMedia> sources = new ArrayList<>();
            Elements sourceElements = overlayElements.select("em");
            ArticleMedia source = new ArticleMedia();

            if (!sourceElements.isEmpty()) {
                Elements aElement = sourceElements.select("a");
                String sourceHref = aElement.attr("href");
                String sourceTitle = aElement.text();
                source.setHref(sourceHref);
                source.setTitle(sourceTitle);
            } else {
                source.setHref(webSiteURL());
                source.setTitle("Slo Android");
            }

            sources.add(source);
            article.setSources(sources);


            overlayElements.select("img").remove();
            overlayElements.select(".itemImage").remove();
            overlayElements.select(".itemImageBlock").remove();
            article.setHtml(overlayElements.html());

            return article;
        } catch (Exception e) {
            logger.log(Level.ERROR, "SloAndroidParserService returnArticle " + e.toString());
            return null;
        }
    }

    private Long formatSloAndroidDate(String dayString, String monthString, String yearString, String timeString) {
        int day = Integer.parseInt(dayString.replace(".", ""));
        int month = 0;
        switch (monthString) {
            case "januar":
                month = 0;
                break;
            case "februar":
                month = 1;
                break;
            case "marec":
                month = 2;
                break;
            case "april":
                month = 3;
                break;
            case "maj":
                month = 4;
                break;
            case "junij":
                month = 5;
                break;
            case "julij":
                month = 6;
                break;
            case "avgust":
                month = 7;
                break;
            case "september":
                month = 8;
                break;
            case "oktober":
                month = 9;
                break;
            case "november":
                month = 10;
                break;
            case "december":
                month = 11;
                break;
        }

        int year = Integer.parseInt(yearString);

        int hours = Integer.parseInt(timeString.split(":")[0]);
        int minutes = Integer.parseInt(timeString.split(":")[1]);

        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.HOUR_OF_DAY, hours);
        calendar.set(Calendar.MINUTE, minutes);

        return calendar.getTimeInMillis();
    }
}
