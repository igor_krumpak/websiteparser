/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package si.iitech.parser;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import si.iitech.model.Article;
import si.iitech.model.ArticleMedia;

import javax.enterprise.context.RequestScoped;
import javax.inject.Singleton;
import java.util.*;

/**
 * @author Igor
 */
@Singleton
public class MobitelTehnikParser extends WebSiteParser {

    private static final Logger logger = Logger.getLogger(MobitelTehnikParser.class);

    @Override
    protected String webSiteURL() {
        return "http://tehnik.mobitel.si";
    }

    public List<Article> parseAllArticles() {
        List<Article> articleList = new ArrayList<>();
        try {
            for (int i = 1; i <= 1; i++) {
                Document document;
                if (i == 1) {
                    document = super.readWebSite("http://tehnik.mobitel.si/novice#k=", false);
                } else {
                    document = super.readWebSite((new StringBuilder("http://tehnik.mobitel.si/page/")).append(i).append("/").toString(), false);
                }
                Elements tableNews = document.select("tr");
                for (Element tr : tableNews) {
                    String articleHref = tr.select("a").attr("href");
                    Article article = returnArticle(articleHref);
                    if (article != null) {
                        logger.log(Level.INFO, article.toJsonString());
                        articleList.add(article);
                    }
                }
            }
            return articleList;
        } catch (Exception e) {
            logger.log(Level.ERROR, "MobitelTehnikParser parseAllArticles" + e.toString());
            return articleList;
        }
    }

    public Article returnArticle(String href) {
        try {

            Document document = super.readWebSite(href, false);
            Article article = new Article();

            Elements overlayElements = document.select(".overlay");

            //HREF
            article.setHref(href);

            //TITLE
            Elements titleElement = overlayElements.select(".txt");
            String title = titleElement.text();
            article.setTitle(title);

            //AUTHOR
            Elements authorElement = overlayElements.select(".author");
            String author = authorElement.text();
            List<String> authors = new ArrayList<String>();
            authors.add(author);
            article.setAuthors(authors);

            //DATE
            Elements dateElement = overlayElements.select(".imgauthor");
            String dateTime = dateElement.text().split("-")[0];
            Date date = formatMobitelTehnikDate(dateTime);
            article.setArticleDate(date.getTime());

            //SOURCE
            article.setSource(Article.Source.MOBITEL_TEHNIK);

            Elements newsElement = document.select("div.content-wrap");

            //SOURCES
            Elements sourceElementsRaw = newsElement.select("em");
            ArrayList<ArticleMedia> sourceList = new ArrayList<>();
            if (!sourceElementsRaw.isEmpty()) {
                Elements sourceElements = sourceElementsRaw.last().select("a");
                for (Element sourceElement : sourceElements) {
                    ArticleMedia source = new ArticleMedia();
                    source.setHref(sourceElement.attr("href"));
                    source.setTitle(sourceElement.text());
                    sourceList.add(source);
                }
                article.setSources(sourceList);
            } else {
                ArticleMedia source = new ArticleMedia();
                source.setTitle("Mobitel Tehnik");
                source.setHref(webSiteURL());
                sourceList.add(source);
            }
            article.setSources(sourceList);

            //IMAGES
            Elements imageElements = newsElement.select("img");
            ArrayList<ArticleMedia> imageList = new ArrayList<>();
            if (!imageElements.isEmpty()) {
                for (Element imageElement : imageElements) {
                    ArticleMedia image = new ArticleMedia();
                    image.setHref(webSiteURL() + imageElement.attr("src"));
                    image.setTitle(imageElement.attr("title"));
                    imageList.add(image);
                }
            }
            article.setImages(imageList);

            //VIDEO ELEMENTS
            Elements videoElements = newsElement.select("div.video-container[unselectable=on]");
            ArrayList<ArticleMedia> videoList = new ArrayList<>();
            if (!videoElements.isEmpty()) {
                for (Element videoElement : videoElements) {
                    ArticleMedia video = new ArticleMedia();
                    String videoHref = videoElement.select("iframe").attr("src");
                    video.setHref(videoHref);
                    videoList.add(video);
                }
            }
            article.setVideos(videoList);

            Element newsElementsDiv = newsElement.select("div").get(1);
            newsElementsDiv.select("div").remove();
            newsElementsDiv.select("img").remove();
            newsElementsDiv.select("iframe").remove();
            newsElementsDiv.select("em").remove();
            article.setHtml(newsElementsDiv.html());
            return article;

        } catch (Exception e) {
            logger.log(Level.ERROR, "MobitelTehnikParser returnArticle " + e.toString());
            return null;
        }
    }

    private Date formatMobitelTehnikDate(String dateRaw) {
        String split[] = dateRaw.split("\\s");
        String stringDay = split[0].replace(".", "");
        int day = Integer.parseInt(stringDay);
        int month = 0;
        if (split[1].contains("januar")) {
            month = 0;
        } else if (split[1].contains("ferbruar")) {
            month = 1;
        } else if (split[1].contains("ferbruar")) {
            month = 2;
        } else if (split[1].contains("marec")) {
            month = 2;
        } else if (split[1].contains("april")) {
            month = 3;
        } else if (split[1].contains("maj")) {
            month = 4;
        } else if (split[1].contains("junij")) {
            month = 5;
        } else if (split[1].contains("julij")) {
            month = 6;
        } else if (split[1].contains("avgust")) {
            month = 7;
        } else if (split[1].contains("september")) {
            month = 8;
        } else if (split[1].contains("oktober")) {
            month = 9;
        } else if (split[1].contains("november")) {
            month = 10;
        } else if (split[1].contains("december")) {
            month = 11;
        }
        int year = Integer.parseInt(split[2]);
        Calendar calendar = new GregorianCalendar(year, month, day);
        return calendar.getTime();
    }
}
