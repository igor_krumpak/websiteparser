package si.iitech.parser;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.net.ssl.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

/**
 * Created by igor on 10.1.2015.
 */

public abstract class WebSiteParser {

    private static final Logger logger = Logger.getLogger(WebSiteParser.class);

    protected WebSiteParser() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };

        // Install the all-trusting trust manager
        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (NoSuchAlgorithmException e) {
            logger.log(Level.ERROR, e);
        } catch (KeyManagementException e) {
            logger.log(Level.ERROR, e);
        }

        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }
    // Install the all-trusting host verifier

    protected Document readWebSite(String urlString, boolean userAgent) {
        Document doc;
        Connection conn;
        try {
            if (userAgent) {
                conn = Jsoup.connect(urlString).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2");
                doc = Jsoup.parse(new String(
                        conn.execute().bodyAsBytes(), "UTF-8"));
            } else {
                conn = Jsoup.connect(urlString);
                doc = Jsoup.parse(new String(
                        conn.execute().bodyAsBytes(), "UTF-8"));
            }
            return doc;
        } catch (MalformedURLException e) {
            logger.log(Level.ERROR, e);
        } catch (UnsupportedEncodingException e) {
            logger.log(Level.ERROR, e);
        } catch (IOException e) {
            logger.log(Level.ERROR, e);
        }
        return null;
    }

    protected abstract String webSiteURL();

}
