package si.iitech.parser;

import si.iitech.db.Couchbase;
import si.iitech.model.Article;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.TimerTask;

/**
 * Created by igor on 13.1.2015.
 */
@Singleton
public class ArticleParserService extends TimerTask {

    @Inject
    private GizzmoNoviceParser gizzmoNoviceParser;

    @Inject
    private SloTechParser sloTechParser;

    @Inject
    private SloAndroidParser sloAndroidParser;

    @Inject
    private RacunalniskeNoviceParser racunalniskeNoviceParser;

    @Inject
    private MobitelTehnikParser mobitelTehnikParser;


    @Inject
    private Couchbase couchbase;

    @Override
    public void run() {
        List<Article> articles = sloTechParser.parseAllArticles();
        //articles.addAll(gizzmoNoviceParser.parseAllArticles());
        //articles.addAll(racunalniskeNoviceParser.parseAllArticles());
        //articles.addAll(sloAndroidParser.parseAllArticles());
        //articles.addAll(mobitelTehnikParser.parseAllArticles());
        couchbase.insert(articles);
    }


}
