/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package si.iitech.parser;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import si.iitech.model.Article;
import si.iitech.model.ArticleMedia;

import javax.inject.Singleton;
import java.util.*;

/**
 * @author Igor
 */
@Singleton
public class GizzmoNoviceParser extends WebSiteParser {

    private static final Logger logger = Logger.getLogger(SloTechParser.class);

    @Override
    protected String webSiteURL() {
        return "http://gizzmo.si";
    }

    public List<Article> parseAllArticles() {
        List<Article> articleList = new ArrayList<>();
        try {
            Document document = super.readWebSite(webSiteURL() + "/novice", false);
            Elements titleElements = document.select(".newstitle");
            Elements hrefElemenets = titleElements.select("a");

            // DATE
            Elements dateHeaderElement = document.select(".headtext");
            Elements dateElement = dateHeaderElement.select(".date");
            String dateText = dateElement.text();
            Date date = formatGizzmoNoviceDate(dateText);


            Article article = returnArticle(webSiteURL() + hrefElemenets.attr("href"), date);
            if (article != null) {
                logger.log(Level.INFO, article.toJsonString());
                articleList.add(article);
            }


            Elements newsElements = document.select(".novicka");
            for (Element e : newsElements) {
                titleElements = e.select(".minititle");
                hrefElemenets = e.select("a");
                dateElement = e.select(".datedva");
                dateText = dateElement.text();
                date = formatGizzmoNoviceDate(dateText);
                article = returnArticle(webSiteURL() + hrefElemenets.attr("href"), date);
                if (article != null) {
                    logger.log(Level.INFO, article.toJsonString());
                    articleList.add(article);
                }
            }
            return articleList;
        } catch (Exception e) {
            logger.log(Level.ERROR, "GizzmoNoviceParser parseAllArticles " + e.toString());
            return articleList;
        }
    }

    public Article returnArticle(String href, Date date) {
        try {
            Document document = super.readWebSite(href, false);
            Article article = new Article();

            //HREF
            article.setHref(href);

            //OVERLAY
            Elements overlayElements = document.select(".contentPage");

            //TITLE
            Elements newsElement = document.select(".headnewsdva");
            Elements titleElement = newsElement.select("h1");
            String title = titleElement.text();
            article.setTitle(title);

            //AUTHOR
            List<String> authors = new ArrayList<String>();
            authors.add("Gizzmo");
            article.setAuthors(authors);


            //SOURCE
            article.setSource(Article.Source.GIZZMO_NOVICE);

            //DATE
            article.setArticleDate(date.getTime());

            //IMAGES
            Elements imageElementsSeperator = overlayElements.select(".headimg");
            Elements imageElementsTable = overlayElements.select(".imageround");

            ArrayList<ArticleMedia> images = new ArrayList<>();
            if (!imageElementsSeperator.isEmpty()) {

                for (Element imageElement : imageElementsSeperator) {
                    ArticleMedia image = new ArticleMedia();
                    String imageHref = webSiteURL() + imageElement.select("img").attr("src");
                    if (imageHref.isEmpty()) {
                        continue;
                    }
                    image.setHref(imageHref);
                    image.setTitle("");
                    images.add(image);
                }
                for (Element imageElement : imageElementsTable) {
                    ArticleMedia image = new ArticleMedia();
                    String imageHref = webSiteURL() + imageElement.select("img").attr("src");
                    if (imageHref.isEmpty()) {
                        continue;
                    }
                    image.setHref(imageHref);
                    image.setTitle(imageElement.select("img").attr("alt"));
                    images.add(image);
                }
            }
            article.setImages(images);

            Elements text = overlayElements.select(".text");
            text.select(".imageround").remove();

            //SOURCES
            ArticleMedia source = new ArticleMedia();
            source.setTitle("Gizzmo");
            source.setHref(webSiteURL());
            List<ArticleMedia> sources = new ArrayList<>();
            sources.add(source);
            article.setSources(sources);

            //VIDEO
            article.setVideos(new ArrayList<ArticleMedia>());

            article.setHtml(text.html());
            return article;
        } catch (Exception e) {
            logger.log(Level.ERROR, "GizzmoNoviceParser returnArticle " + e.toString());
            return null;
        }
    }

    private Date formatGizzmoNoviceDate(String text) {
        String[] dateTextArray = text.split("\\.");
        int day = Integer.parseInt(dateTextArray[0].trim());
        int month = Integer.parseInt(dateTextArray[1].trim()) - 1;
        int year = Integer.parseInt(dateTextArray[2].trim());
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.YEAR, year);
        return calendar.getTime();
    }
}
