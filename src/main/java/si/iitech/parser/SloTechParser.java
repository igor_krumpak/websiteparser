package si.iitech.parser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import si.iitech.model.Article;
import si.iitech.model.ArticleMedia;

import javax.inject.Singleton;

/**
 * Created by igor on 10.1.2015.
 */
@Singleton
public class SloTechParser extends WebSiteParser  {

    private static final Logger logger = Logger.getLogger(SloTechParser.class);

    @Override
    protected String webSiteURL() {
        return "https://slo-tech.com";
    }

    public List<Article> parseAllArticles() {
        List<Article> articleList = new ArrayList<>();
        try {
            Document document = super.readWebSite(webSiteURL(), true);
            Elements freshNewsElements = document.select("#fresh_news");
            Elements newsElements = freshNewsElements.select("[href~=novice]");
            for (Element element : newsElements) {
                String partialHref = element.attr("href");
                String href = webSiteURL() + partialHref + "/0";
                Article article = parseOneArticle(href);
                if (article != null) {
                    logger.log(Level.INFO, article.toJsonString());
                    articleList.add(article);
                }
            }
            return articleList;
        } catch (Exception e) {
            logger.log(Level.ERROR, "SloTechParser parseAllArticles " + e.toString());
            return articleList;
        }
    }



    private Article parseOneArticle(String href) {
        try {

            Document document = super.readWebSite(href, true);

            Article article = new Article();
            Elements elementArticle = document.select("article");

            //SOURCE
            article.setSource(Article.Source.SLO_TECH);

            //HREF
            article.setHref(href);

            //TITLE
            String title = elementArticle.select("[itemprop=name]").text();
            if (title.isEmpty()) {
                return null;
            }
            article.setTitle(title);

            //DATETIME
            String dateTime = elementArticle.select("[itemprop=datePublished]").attr("datetime");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String[] dateTextArray = dateTime.split("T");
            Date finalDate = sdf.parse(dateTextArray[0]);
            article.setArticleDate(finalDate.getTime());

            //AUTHOR
            String author = elementArticle.select("[itemprop=author]").text();
            List<String> authors = new ArrayList<String>();
            authors.add(author);
            article.setAuthors(authors);

            //IMAGES
            Elements imageElements = elementArticle.select("img");
            ArticleMedia image;
            List<ArticleMedia> images = new ArrayList();
            if (!imageElements.isEmpty()) {
                for (Element imageElement : imageElements) {
                    image = new ArticleMedia();
                    image.setHref((new StringBuilder("https:")).append(imageElement.attr("src")).toString());
                    image.setTitle(imageElement.attr("alt").trim());
                    images.add(image);
                }
            }
            article.setImages(images);

            //VIDEOS
            Elements videoElements = elementArticle.select(".youtube-player");
            List<ArticleMedia> videos = new ArrayList();
            if (!videoElements.isEmpty()) {
                for (Element videoElement : videoElements) {
                    ArticleMedia video = new ArticleMedia();
                    video.setHref(videoElement.attr("src"));
                    videos.add(video);
                }
            }
            article.setVideos(videos);

            //SOURCE
            Elements elementsData = elementArticle.select(".besediloNovice");
            elementsData.select("div").remove();
            Elements sourceElements = elementsData.select(".source");
            ArrayList<ArticleMedia> sources = new ArrayList();
            if (!sourceElements.isEmpty()) {
                ArticleMedia source = new ArticleMedia();
                source.setHref(sourceElements.get(0).attr("href"));
                source.setTitle(sourceElements.get(0).text());
                sources.add(source);
            }
            article.setSources(sources);

            //REMOVE UNWANTED DATA
            elementsData.select(".source").remove();
            elementsData.select("iframe").remove();

            //HTML
            String data = elementsData.html();
            article.setHtml(data.substring(2));

            return article;
        } catch (Exception e) {
            logger.log(Level.ERROR, "SloTechParserService returnArticle " + e.toString());
            return null;
        }
    }
}
