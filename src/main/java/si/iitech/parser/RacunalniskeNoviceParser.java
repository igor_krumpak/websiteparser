/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package si.iitech.parser;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import si.iitech.model.Article;
import si.iitech.model.ArticleMedia;

import javax.inject.Singleton;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Igor
 */
@Singleton
public class RacunalniskeNoviceParser extends WebSiteParser {

    private static final Logger logger = Logger.getLogger(RacunalniskeNoviceParser.class);

    @Override
    protected String webSiteURL() {
        return "http://www.racunalniske-novice.com/novice/";
    }

    public List<Article> parseAllArticles() {
        List<Article> articleList = new ArrayList<>();
        try {
            for (int i = 0; i < 2; i++) {
                Document document;
                if (i == 0) {
                    document = super.readWebSite(webSiteURL(), true);
                } else {
                    document = super.readWebSite((new StringBuilder(webSiteURL() + "listaj-arhiv/")).append(i).append("/").toString(), true);
                }
                Elements freshNewsElements = document.select(".latest-dotted-nl");
                for (Element freshNewsElement : freshNewsElements) {
                    String href = freshNewsElement.select("a").first().attr("href");
                    Article article = returnArticle(href);
                    if (article != null) {
                        logger.log(Level.INFO, article.toJsonString());
                        articleList.add(article);
                    }
                }
            }
            return articleList;
        } catch (Exception e) {
            logger.log(Level.ERROR, "RacunalniskeNoviceParserService parseAllArticles " + e.toString());
            return articleList;
        }
    }

    private Article returnArticle(String href) {
        try {
            Document document = super.readWebSite(href, true);

            Elements articleElement = document.select("div#_iprom_inStream");
            Article article = new Article();

            //HREF
            article.setHref(href);

            //SOURCE
            article.setSource(Article.Source.RACUNALNISKE_NOVICE);

            //AUTHOR
            List<String> authors = new ArrayList<String>();
            authors.add("Računalniške Novice");
            article.setAuthors(authors);

            //TITLE
            String title = articleElement.select("h1").text();

            article.setTitle(title);


            //DATE
            String dateTime = document.select(".single-art-date.fl.mr10").text();
            dateTime = dateTime.replaceAll("\u00a0", "");
            Date date = formatDateTimeString(dateTime);

            article.setArticleDate(date.getTime());

            //IMAGES
            Elements imageElements = articleElement.select("img[src$=.jpg]");
            ArrayList<ArticleMedia> images = new ArrayList<>();
            if (!imageElements.isEmpty()) {
                for (Element imageElement : imageElements) {
                    ArticleMedia mediaElement = new ArticleMedia();
                    mediaElement.setHref(webSiteURL().replace("novice/", "") + imageElement.attr("src"));
                    mediaElement.setTitle(imageElement.attr("alt"));
                    images.add(mediaElement);
                }
            }
            article.setImages(images);


            //VIDEOS
            Elements videoElements = articleElement.select("embed");
            ArrayList<ArticleMedia> videos = new ArrayList<>();
            if (!videoElements.isEmpty()) {
                for (Element videoElement : videoElements) {
                    ArticleMedia mediaElement = new ArticleMedia();
                    String stringHref = videoElement.attr("src").replace("//", "");
                    mediaElement.setHref("http://" + stringHref);
                    videos.add(mediaElement);
                }
            }
            article.setVideos(videos);

            //SOURCES
            Elements linksContainer = articleElement.select("div.single-art-linkage");
            Elements linkElements = linksContainer.select("a");
            ArrayList<ArticleMedia> linkList = new ArrayList<>();
            if (!linkElements.isEmpty()) {
                for (Element linkElemenet : linkElements) {
                    ArticleMedia mediaElement = new ArticleMedia();
                    mediaElement.setHref(linkElemenet.attr("href"));
                    mediaElement.setTitle(linkElemenet.text());
                    linkList.add(mediaElement);
                }
            } else {
                ArticleMedia source = new ArticleMedia();
                source.setHref(webSiteURL());
                source.setTitle("Računalniške Novice");
            }
            article.setSources(linkList);

            //DATA
            Elements dataElement = articleElement.select("div.single-art-text");
            dataElement.select("div").remove();
            dataElement.select("script").remove();
            article.setHtml(dataElement.outerHtml());

            return article;
        } catch (Exception e) {
            logger.log(Level.ERROR, "RacunalniskeNoviceParserService returnArticle " + e.toString());
            return null;
        }

    }

    private Date formatDateTimeString(String dateTime) {
        try {
            if (dateTime.length() == 15) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyyHH:mm");
                return sdf.parse(dateTime);
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyyH:mm");
                return sdf.parse(dateTime);
            }
        } catch (ParseException e) {
            logger.log(Level.ERROR, "RacunalniskeNoviceParserService formatDateTimeString" + e.toString());
            return new Date();
        }
    }
}