package si.iitech.model;

import com.google.gson.Gson;
import com.mongodb.util.JSON;
import org.bson.BSONObject;

/**
 * Created by igor on 13.1.2015.
 */
public class ArticleMedia extends Json{

    private String title;
    private String href;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public String toString() {
        return "ArticleMedia{" +
                "title='" + title + '\'' +
                ", href='" + href + '\'' +
                '}';
    }


}
