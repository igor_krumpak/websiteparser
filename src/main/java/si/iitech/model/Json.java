package si.iitech.model;

import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.transcoder.JsonTranscoder;
import com.google.gson.Gson;
import com.mongodb.util.JSON;
import org.bson.BSONObject;

/**
 * Created by igor on 18.1.2015.
 */
public class Json {

    protected String title;
    protected String _id;

    public String toJsonString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public JsonObject toJsonObject() {
        JsonTranscoder transcoder = new JsonTranscoder();
        JsonObject jsonObject = null;
        try {
            jsonObject = transcoder.stringToJsonObject(this.toJsonString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String uniqueKeyGenerator(String text) {
        int hash = 0;
        for (int i = 0; i < text.length(); i++) {
            hash = (hash << 5) - hash + text.charAt(i);
        }
        return String.valueOf(Math.abs(hash));
    }

    public String get_id() {
        return _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        this._id = uniqueKeyGenerator(title);
    }

}
