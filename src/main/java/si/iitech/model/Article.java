package si.iitech.model;

import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.transcoder.JsonTranscoder;
import com.google.gson.Gson;
import com.mongodb.util.JSON;
import org.bson.BSONObject;

import java.util.Date;
import java.util.List;

/**
 * Created by igor on 8.1.2015.
 */
public class Article extends Json {

    public enum Source {
        SLO_TECH, SLO_ANDROID, MOBITEL_TEHNIK, GIZZMO_NOVICE, RACUNALNISKE_NOVICE
    }

    private Source source;
    private Long articleDate;
    private String href;
    private String html;
    private List<String> authors;
    private List<ArticleMedia> sources;
    private List<ArticleMedia> images;
    private List<ArticleMedia> videos;

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public void setArticleDate(Long articleDate) {
        this.articleDate = articleDate;
    }

    public Long getArticleDate() {
        return articleDate;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public List<ArticleMedia> getSources() {
        return sources;
    }

    public void setSources(List<ArticleMedia> sources) {
        this.sources = sources;
    }

    public List<ArticleMedia> getImages() {
        return images;
    }

    public void setImages(List<ArticleMedia> images) {
        this.images = images;
    }

    public List<ArticleMedia> getVideos() {
        return videos;
    }

    public void setVideos(List<ArticleMedia> videos) {
        this.videos = videos;
    }

    @Override
    public String toString() {
        return "Article{" +
                "title='" + title + '\'' +
                ", source=" + source +
                ", articleDate=" + articleDate +
                ", href='" + href + '\'' +
                ", authors=" + authors +
                ", sources=" + sources +
                ", images=" + images +
                ", videos=" + videos +
                '}';
    }
}
