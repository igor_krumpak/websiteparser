package si.iitech.db;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.HTTP;
import si.iitech.model.Article;

import javax.inject.Singleton;
import java.util.List;

/**
 * Created by igor on 29.1.2015.
 */
@Singleton
public class Couchbase {

    private static final Logger logger = Logger.getLogger(Couchbase.class);

    public void insert(List<Article> articles) {
        for (final Article article : articles) {
            Unirest.post("http://178.62.148.159:4984/grocery-sync/")
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .body(article.toJsonString())
                    .asJsonAsync(new Callback<JsonNode>() {
                        @Override
                        public void completed(HttpResponse<JsonNode> response) {
                            if(response.getStatus() != 200 && response.getStatus() != 409) {
                                logger.log(Level.ERROR, "Article " + article.getTitle() + " was not saved, status code: " + response.getStatus());
                            }
                        }

                        @Override
                        public void failed(UnirestException e) {
                            logger.log(Level.ERROR, e.toString());
                        }

                        @Override
                        public void cancelled() {}
                    });
        }
    }
}
