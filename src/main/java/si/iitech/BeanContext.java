package si.iitech;


import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

/**
 * Created by igor on 13.1.2015.
 */
public class BeanContext {
    public static final BeanContext INSTANCE = new BeanContext();

    private final Weld weld;
    private final WeldContainer container;

    private BeanContext() {
        this.weld = new Weld();
        this.container = weld.initialize();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                weld.shutdown();
            }
        });
    }

    public <T> T getBean(Class<T> type) {
        return container.instance().select(type).get();
    }
}
